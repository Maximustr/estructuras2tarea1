﻿namespace Estructuras2.Tarea1.Entities
{
    public class Node
    {
        public Node(int val)
        {
            Number = val;
        }

        public Node NextNode { get; set; }

        public Node LeftNode { get; set; }

        public Node RigthNode { get; set; }

        public int Number { get; set; }

        public Node GetLastNode() => GetLastNode(this);

        public bool HasNext() => NextNode != null;

        public string Print(string structureName) => $"All the elements in the {structureName} are: {PrintAll(this)}";

        private static Node GetLastNode(Node nextNode) =>
                    nextNode.NextNode == null ? nextNode : GetLastNode(nextNode.NextNode);

        private static string PrintAll(Node nextNode) =>
            nextNode.NextNode == null ? $"{nextNode.Number} " : $"{nextNode.Number} {PrintAll(nextNode.NextNode)}";
    }
}