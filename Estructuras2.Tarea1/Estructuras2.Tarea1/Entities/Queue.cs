﻿using System;
using Estructuras2.Tarea1.Abstact;

namespace Estructuras2.Tarea1.Entities
{
    public class Queue : IStructure
    {
        public Node HeadNode { get; set; }

        public void Add(int value)
        {
            var newNode = new Node(value);
            if (HeadNode == null)
            {
                HeadNode = newNode;
                return;
            }
            newNode.NextNode = HeadNode;
            HeadNode = newNode;
        }

        public void Delete(int number = 0)
        {
            if (HeadNode == null) return;
            if (!HeadNode.HasNext())
            {
                HeadNode = null;
                return;
            }
            var tempData = HeadNode;
            while (tempData != null && tempData.HasNext())
            {
                if (!tempData.NextNode.HasNext())
                    tempData.NextNode = null;
                tempData = tempData.NextNode;
            }
        }

        public Node GetNode(int number = 0)
        {
            return HeadNode?.NextNode;
        }

        public string Print() => HeadNode?.Print("queue");

        public void Move(IStructure initialStructure, IStructure finalStructure, int number = 0)
        {
            if (initialStructure == finalStructure)
                throw new InvalidOperationException();

            var node = initialStructure.GetNode();

            initialStructure.Delete(node.Number);
            finalStructure.Add(node.Number);
        }
    }
}