﻿using System;
using Estructuras2.Tarea1.Abstact;

namespace Estructuras2.Tarea1.Entities
{
    public class List : IStructure
    {
        public Node HeadNode { get; set; }

        public void Add(int value)
        {
            var newNode = new Node(value);

            if (HeadNode == null)
                HeadNode = newNode;
            else if (HeadNode.Number > newNode.Number)
            {
                newNode.NextNode = HeadNode;
                HeadNode = newNode;
            }
            else if (!HeadNode.HasNext())
                HeadNode.NextNode = newNode;
            else
                Add(newNode);
        }

        public void Delete(int number)
        {
            if (HeadNode == null) return;

            if (HeadNode.Number == number)
            {
                HeadNode = HeadNode.HasNext() ? HeadNode.NextNode : null;
                return;
            }

            var tempNode = HeadNode;
            while (tempNode.HasNext())
            {
                if (tempNode.NextNode.Number == number)
                {
                    tempNode.NextNode = tempNode.NextNode.HasNext() ? tempNode.NextNode.NextNode : null;
                    return;
                }
                tempNode = tempNode.NextNode;
            }
        }

        public string Print() => HeadNode?.Print("list");

        private void Add(Node newNode)
        {
            var tempNode = HeadNode;

            while (tempNode.HasNext())
            {
                if (tempNode.NextNode.Number > newNode.Number)
                {
                    newNode.NextNode = tempNode.NextNode;
                    tempNode.NextNode = newNode;
                    return;
                }
                tempNode = tempNode.NextNode;
            }
            tempNode.NextNode = newNode;
        }

        public Node GetNode(int number = 0)
        {
            while (HeadNode.HasNext())
            {
                if (HeadNode.NextNode.Number == number)
                    return HeadNode.NextNode;
            }

            throw new InvalidOperationException("The number " + number + " doesn't exist.");
        }

        public void Move(IStructure initialStructure, IStructure finalStructure, int number = 0)
        {
            if (initialStructure == finalStructure)
                throw new InvalidOperationException();

            var node = initialStructure.GetNode(number);

            initialStructure.Delete(node.Number);
            finalStructure.Add(node.Number);
        }
    }
}