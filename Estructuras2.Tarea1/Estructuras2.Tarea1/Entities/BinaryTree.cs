﻿using Estructuras2.Tarea1.Abstact;

namespace Estructuras2.Tarea1.Entities
{
    public class BinaryTree : IStructure
    {
        public Node Root { get; set; }
        public string Tree { get; set; }

        public BinaryTree()
        {
            Root = null;
        }

        public void Add(int value)
        {
            var newNode = new Node(value);

            if (Root == null)
            {
                Root = newNode;
                return;
            }

            InsertRec(Root, newNode);
        }

        private static void InsertRec(Node root, Node newNode)
        {
            while (true)
            {
                if (root == null)
                    root = newNode;

                if (newNode.Number < root.Number)
                {
                    if (root.LeftNode == null)
                        root.LeftNode = newNode;
                    else
                    {
                        root = root.LeftNode;
                        continue;
                    }
                }
                else
                {
                    if (root.RigthNode == null)
                        root.RigthNode = newNode;

                    else
                    {
                        root = root.RigthNode;
                        continue;
                    }
                }

                break;
            }
        }

        private string Print(Node root)
        {
            while (true)
            {
                if (root == null)
                    return Tree;

                Print(root.LeftNode);

                Tree += root.Number + " - ";

                root = root.RigthNode;
            }
        }


        public string RecursiveInorder(Node root)
        {
            var inorderTree = "";

            while (true)
            {
                if (root.LeftNode != null)
                    RecursiveInorder(root.LeftNode);

                inorderTree += root.Number.ToString();

                if (root.RigthNode != null)
                {
                    root = root.RigthNode;
                    continue;
                }

                break;
            }

            return inorderTree;
        }

        public void Delete(int number = 0)
        {
            throw new System.NotImplementedException();
        }

        public Node GetNode(int number = 0)
        {
            throw new System.NotImplementedException();
        }

        public void Move(IStructure initiaStructure, IStructure finalStructure, int number = 0)
        {
            throw new System.NotImplementedException();
        }

        public string Print()
        {
            return Print(Root);
        }
    }
}
