﻿using System;
using Estructuras2.Tarea1.Abstact;
using Estructuras2.Tarea1.Entities;

namespace Estructuras2.Tarea1
{
    public class HomeWork
    {
        public HomeWork()
        {
            Queue = new Queue();
            Heap = new Heap();
            List = new List();
            BinaryTree = new BinaryTree();
        }

        private static Heap Heap { get; set; }
        private static List List { get; set; }
        private static Queue Queue { get; set; }
        private static BinaryTree BinaryTree { get; set; }

        public void MenuOfInteracion(IStructure structure)
        {
            while (true)
            {
                Console.WriteLine("Enter your option");
                Console.WriteLine("1. Add");
                Console.WriteLine("2. Delete");
                Console.WriteLine("3. Move");
                Console.WriteLine("4. Print");
                Console.WriteLine("5. Finish");
                var selectedOption = Console.ReadLine();

                switch (selectedOption)
                {
                    case "1":
                        Console.WriteLine("Add a number");
                        structure.Add(int.Parse(Console.ReadLine() ?? throw new InvalidOperationException()));
                        break;

                    case "2":
                        if (structure is List)
                        {
                            Console.WriteLine("Which number do you want to delete?");
                            structure.Delete(int.Parse(Console.ReadLine() ?? throw new InvalidOperationException()));
                        }
                        else
                            structure.Delete();

                        Console.WriteLine("Number deleted");
                        break;

                    case "3":
                        var number = 0;

                        if (structure == List)
                        {
                            Console.WriteLine(List.Print());
                            Console.WriteLine("Which number of the list do you want to move?");
                            number = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                        }

                        Console.WriteLine("To which structure do you want to move the number?");
                        Console.WriteLine("1. Queue");
                        Console.WriteLine("2. Stack");
                        Console.WriteLine("3. List");

                        switch (Console.ReadLine())
                        {
                            case "1":
                                structure.Move(structure, Queue);
                                break;

                            case "2":
                                structure.Move(structure, Heap);
                                break;

                            default:
                                
                                structure.Move(structure, List, number);
                                break;
                        }
                        break;

                    case "4":
                        Console.WriteLine(structure.Print());
                        break;

                    case "5":
                        return;

                    default:
                        Console.WriteLine("Enter a valid option.");
                        break;
                }
            }
        }

        public void Start()
        {
            while (true)
            {
                Console.WriteLine("Enter your option");
                Console.WriteLine("1. Queue");
                Console.WriteLine("2. Hip");
                Console.WriteLine("3. List");
                Console.WriteLine("4. Binary Tree");
                Console.WriteLine("5. Finish");
                var selectedOption = Console.ReadLine();

                switch (selectedOption)
                {
                    case "1":
                        MenuOfInteracion(Queue);
                        break;

                    case "2":
                        MenuOfInteracion(Heap);
                        break;

                    case "3":
                        MenuOfInteracion(List);
                        break;

                    case "4":
                        MenuOfInteracion(BinaryTree);
                        break;

                    case "5":
                        return;

                    default:
                        Console.WriteLine("Enter a valid option.");
                        break;
                }
            }
        }
    }
}