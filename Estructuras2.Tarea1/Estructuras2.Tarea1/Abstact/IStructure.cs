﻿using Estructuras2.Tarea1.Entities;

namespace Estructuras2.Tarea1.Abstact
{
    public interface IStructure
    {
        void Add(int value);

        void Delete(int number = 0);

        Node GetNode(int number = 0);

        void Move(IStructure initiaStructure, IStructure finalStructure, int number = 0);

        string Print();
    }
}